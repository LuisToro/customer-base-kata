package it.xpug.ocp.customerbase;

public enum findType {
	findByLastName, findByFirstAndLastName, findByCreditGreaterThan;
}
