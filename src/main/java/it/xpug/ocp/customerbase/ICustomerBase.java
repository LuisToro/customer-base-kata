package it.xpug.ocp.customerbase;

import java.util.List;

public abstract class ICustomerBase {
	protected findType type;
	protected abstract List<Customer> find(Customer customers);
}
